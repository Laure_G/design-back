<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[Route('/api/comment')]
class CommentController extends AbstractController
{
    private CommentRepository $repo;

    public function __construct(CommentRepository $repo) {
    	$this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all() {
        
        $comments = $this->repo->findAll();
        return $this->json($comments);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $comment = $this->repo->findById($id);
        if(!$comment){
            throw new NotFoundHttpException();
        }
        return $this->json($comment);
    }

    #[Route('/{idArticle}', methods: 'POST')]
    public function add(int $idArticle,Request $request, SerializerInterface $serializer,ValidatorInterface $validator) {
         try {

            /**
             * @var User
             */
            $user = $this->getUser();
        $comment = $serializer->deserialize($request->getContent(), Comment::class, 'json');
        $comment->setIdUser($user->getId());
        $comment->setIdArticle($idArticle);
        $comment->setAuthor($user->getName());
        $comment->setDate(New \DateTime);
        $this->repo->persist($comment);

        return $this->json($comment, Response::HTTP_CREATED);
    } 
        catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        } 
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $comment = $this->repo->findById($id);
        if (!$comment) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Comment::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function supp(int $id) {
       $comment = $this->repo->deleteById($id);
       if(!$comment) {
        throw new NotFoundHttpException();
        }
        return $this->json($comment,204);
    }
}

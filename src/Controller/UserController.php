<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


#[Route('/api/user')]
class UserController extends AbstractController
{

    public function __construct(private UserRepository $repo) {}

    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $user = $this->repo->findById($id);
        if(!$user){
            throw new NotFoundHttpException();

        }
        return $this->json($user);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $user = $this->repo->findById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }

        $this->repo->delete($user);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}/promote', methods: 'PATCH')]
    public function promote(int $id, Request $request): JsonResponse
    {
        $user = $this->repo->findById($id);
        if(!$user) {
            throw new NotFoundHttpException('User does not exist');
        }
        $user->setRole('ROLE_ADMIN');
        $this->repo->update($user);
        
        return $this->json($user);
    }

  

}
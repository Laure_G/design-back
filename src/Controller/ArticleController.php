<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\User;
use App\Repository\ArticleRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[Route('/api/article')]
class ArticleController extends AbstractController
{
    private ArticleRepository $repo;

    public function __construct(ArticleRepository $repo)
    {
        $this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all()
    {
        $articles = $this->repo->findAll();

        return $this->json($articles);
    }

    #[Route('/popular', methods: 'GET')]
    public function allPopular()
    {
        $articles = $this->repo->findByPopularity();

        return $this->json($articles);
    }



    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $article = $this->repo->findById($id);
        $this->repo->nombreVues($article);

        /**
         * @var User
         * */
        $user = $this->getUser();
        if ($user && array_search($user->getId(), $article->getLikes())) {
            $article->setUserLiked(true);
        }

        if (!$article) {
            throw new NotFoundHttpException();
        }
        return $this->json($article);
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $article = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $article->setDate(new DateTime);
            $this->repo->persist($article);

            return $this->json($article, Response::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }


    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $article = $this->repo->findById($id);
        if (!$article) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }


    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $article = $this->repo->findById($id);
        if (!$article) {
            throw new NotFoundHttpException();
        }

        $this->repo->delete($article);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}/like', methods: 'POST')]
    public function toggleLike(int $id)
    {
        /**
         * @var User
         * */
        $user = $this->getUser();
        $liked = $this->repo->toggleLike($user->getId(), $id);
        return $this->json(['liked' => $liked]);
    }


}
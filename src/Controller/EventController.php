<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Repository\EventRepository;
use App\Service\Uploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[Route('/api/event')]
class EventController extends AbstractController
{
    private EventRepository $repo;

    public function __construct(EventRepository $repo) {
    	$this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all() {
        $event = $this->repo->findAll();
        return $this->json($event);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $event = $this->repo->findById($id);
        if(!$event){
            throw new NotFoundHttpException();
        }
        return $this->json($event);
    }

   
    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator,Uploader $uploader)
    {
        try {
            $event = $serializer->deserialize($request->getContent(), Event::class, 'json');
            $event->setImage( $uploader->upload($event->getImage()));
            $event->setDate(New \DateTime);
            $this->repo->persist($event);

            return $this->json($event, Response::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }
      

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator,Uploader $uploader)
    {
        $event = $this->repo->findById($id);
        if (!$event) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Event::class, 'json');
            $toUpdate->setId($id);
            if(!str_ends_with($toUpdate->getImage(), 'jpg') && !str_starts_with($toUpdate->getImage(), 'http')){
                $toUpdate->setImage( $uploader->upload($toUpdate->getImage()));
            }
            $this->repo->update($toUpdate);
            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }


    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $event = $this->repo->findById($id);
        if (!$event) {
            throw new NotFoundHttpException();
        }

        $this->repo->delete($event);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{idEvent}/subscribe', methods: 'POST')]
    public function addSubcription(int $idEvent)
    {
             /**
             * @var User
             */
            $user = $this->getUser();
            $subscribe = $this->repo->isSubscribe($user->getId(), $idEvent);  
            if (!$subscribe) {
                $this->repo->subscribe($user->getId(), $idEvent); 
                return $this->json(null, Response::HTTP_NO_CONTENT);
            }    else   {
                return $this->json('Vous êtes déjà inscrit', Response::HTTP_BAD_REQUEST);
            }  

    }

    #[Route('/{idEvent}/unsubscribe', methods: 'DELETE')]
    public function deleteSubcription(int $idEvent)
    {
             /**
             * @var User
             */
            $user = $this->getUser();
            $subscribe = $this->repo->isSubscribe($user->getId(), $idEvent);  
            if ($subscribe) {
                $this->repo->unsubscribe($user->getId(), $idEvent); 
                return $this->json(null, Response::HTTP_NO_CONTENT);
            }    else   {
                return $this->json("Vous n'êtes déjà plus inscrit", Response::HTTP_BAD_REQUEST);
            }  

    }

    #[Route('/{idEvent}/subscribe', methods: 'GET')]
    public function isSubcribed(int $idEvent)
    {
             /**
             * @var User
             */
            $user = $this->getUser();
            $subscribe = $this->repo->isSubscribe($user->getId(), $idEvent);  
            if ($subscribe) {
                return $this->json(true);
            }    else   {
                return $this->json(false);
            }  

    }
}

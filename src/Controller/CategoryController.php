<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[Route('/api/category')]
class CategoryController extends AbstractController
{
    private CategoryRepository $repo;

    public function __construct(CategoryRepository $repo) {
    	$this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all() {
        
        $categorys = $this->repo->findAll();
        return $this->json($categorys);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $category = $this->repo->findById($id);
        if(!$category){
            throw new NotFoundHttpException();

        }
        return $this->json($category);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {
         try {
        $category = $serializer->deserialize($request->getContent(), Category::class, 'json');
        $this->repo->persist($category);

        return $this->json($category, Response::HTTP_CREATED);
         } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator ){
        
        $category = $this->repo->findById($id);
        if (!$category) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Category::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function supp(int $id) {
       $category = $this->repo->deleteById($id);
       if(!$category) {
        throw new NotFoundHttpException();
        }
        return $this->json($category,204);
    }
}

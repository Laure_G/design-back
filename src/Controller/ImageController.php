<?php

namespace App\Controller;

use App\Entity\Image;
use App\Repository\ImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\Uploader;



#[Route('/api/image')]
class ImageController extends AbstractController
{
    private ImageRepository $repo;

    public function __construct(ImageRepository $repo) {
    	$this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all() {
        
        $images = $this->repo->findAll();
        return $this->json($images);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $image = $this->repo->findById($id);
        if(!$image){
            throw new NotFoundHttpException();

        }
        return $this->json($image);
    }

    #[Route('/article/{id}', methods:'GET')]
    public function articleImages(int $id){
        $images = $this->repo->findImageByArticle($id);
        return $this->json($images);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, Uploader $uploader) {
        try {
        $image = $serializer->deserialize($request->getContent(), Image::class, 'json');
        $image->setLink( $uploader->upload($image->getLink()));
        $this->repo->persist($image);

        return $this->json($image, Response::HTTP_CREATED);
    } catch (ValidationFailedException $e) {
        return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
    } catch (NotEncodableValueException $e) {
        return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
    }
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $image = $this->repo->findById($id);
        if (!$image) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Image::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function supp(int $id) {
       $image = $this->repo->deleteById($id);
       if(!$image) {
        throw new NotFoundHttpException();
        }
        return $this->json($image,204);
    }
}

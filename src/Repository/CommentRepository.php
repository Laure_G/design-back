<?php

namespace App\Repository;


use App\Entity\Comment;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $comments = [];
        $statement = $this->connection->prepare('SELECT * FROM comment');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $comments[] = $this->sqlToComment($item);
        }
        return $comments;
    }

    public function findById(int $id):?Comment {
        $statement = $this->connection->prepare('SELECT * FROM comment WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToComment($result);
        }
        return null;
    }

    private function sqlToComment(array $line):Comment {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Comment($date ,$line['content'], $line['id_article'], $line['id_user'], $line['id']);
    }

    public function persist(Comment $comment) {
        $statement = $this->connection->prepare('INSERT INTO comment (date,content, id_article, id_user) VALUES (:date,:content, :idArticle, :idUser)');
        $statement->bindValue(':date', $comment->getDate()->format('Y-m-d'));
        $statement->bindValue(':content', $comment->getContent());
        $statement->bindValue(':idArticle', $comment->getIdArticle(), PDO::PARAM_INT);
        $statement->bindValue(':idUser', $comment->getIdUser(), PDO::PARAM_INT);

        $statement->execute();

        $comment->setId($this->connection->lastInsertId());
    }


    public function update(Comment $comment)
    {
        $statement = $this->connection->prepare("UPDATE comment SET date=:date,content=:content,id_article=:idArticle, id_user=:idUser WHERE id=:id");
        $statement->bindValue(":id", $comment->getId(), PDO::PARAM_INT);
        $statement->bindValue(':date', $comment->getDate()->format('Y-m-d'));
        $statement->bindValue(':content', $comment->getContent());
        $statement->bindValue(':idArticle', $comment->getIdArticle(), PDO::PARAM_INT);
        $statement->bindValue(':idUser', $comment->getIdUser(), PDO::PARAM_INT);
        $statement->execute();
        return new JsonResponse(null, 204);
    }

    public function deleteById(int $id):bool
    {
        $statement = $this->connection->prepare('DELETE FROM comment WHERE id=:id');
        $statement->bindValue(':id', $id);
        $results= $statement->execute();
        return $results;
    }

     /**
     * @return Comment[]
     */
    public function findCommentByArticle(int $id): array
    {
        $list = [];
        $query = $this->connection->prepare("SELECT * FROM comment 
        LEFT JOIN  user ON comment.id_user=user.id
        WHERE comment.id_article =:id");

     
        $query->bindValue(':id', $id);
        $query->execute();

    
        foreach ($query->fetchAll() as $line) {
            $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
            $comm = new Comment($date ,$line['content'], $line['id_article'], $line['id_user'], $line['id']);
            $comm->setAuthor( $line ['name']);
            $list[] = $comm;
        }

        return $list;
    }


}


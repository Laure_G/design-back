<?php

namespace App\Repository;

use App\Entity\Article;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class ArticleRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $imageRepo = new ImageRepository();
        $categoryRepo = new CategoryRepository();
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article ORDER BY date DESC');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $images = $imageRepo->findImageByArticle($item['id']);
            $categories = $categoryRepo->findCategoryByArticle($item['id']);
            $article = $this->sqlToArticle($item);
            $article->setImages($images);
            $article->setCategories($categories);
            $likes = $this->findLikesByArticle($article->getId());
            $article->setLikes($likes);
            $articles[] = $article;
        }
        return $articles;
    }

    public function findById(int $id): ?Article
    {
        $imageRepo = new ImageRepository();
        $categoryRepo = new CategoryRepository();
        $commentRepo = new CommentRepository();
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue(':id', $id);

        $statement->execute();

        $result = $statement->fetch();
        $images = $imageRepo->findImageByArticle($result['id']);
        $comments = $commentRepo->findCommentByArticle($result['id']);
        $categories = $categoryRepo->findCategoryByArticle($result['id']);
        $article = $this->sqlToArticle($result);
        $article->setImages($images);
        $article->setComments($comments);
        $article->setCategories($categories);
        $likes = $this->findLikesByArticle($article->getId());
        $article->setLikes($likes);
        if ($result) {
            return $article;
        }
        return null;
    }

    private function sqlToArticle(array $line): Article
    {
        $date = null;
        if (isset($line['date'])) {
            $date = new DateTime($line['date']);
        }
        return new Article($line['title'], $line['content'], $line['author'], $date, $line['views'], $line['id']);
    }

    public function persist(Article $article)
    {
        $statement = $this->connection->prepare('INSERT INTO article (title,content,author,date,views) VALUES (:title, :content, :author, :date, :views)');
        $statement->bindValue(':title', $article->getTitle());
        $statement->bindValue(':content', $article->getContent());
        $statement->bindValue(':author', $article->getAuthor());
        $statement->bindValue(':date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue(':views', $article->getViews(), PDO::PARAM_INT);

        $statement->execute();

        $article->setId($this->connection->lastInsertId());
    }


    public function update(Article $article)
    {
        $statement = $this->connection->prepare("UPDATE article SET title = :title, content=:content, author=:author, date=:date,views=:views WHERE id=:id");
        $statement->bindValue(":id", $article->getId(), PDO::PARAM_INT);
        $statement->bindValue(":title", $article->getTitle());
        $statement->bindValue(":content", $article->getContent());
        $statement->bindValue(":author", $article->getAuthor());
        $statement->bindValue(":date", $article->getDate()->format('Y-m-d'));
        $statement->bindValue(":views", $article->getViews(), PDO::PARAM_INT);

        $statement->execute();
        return new JsonResponse(null, 204);
    }


    public function delete(Article $article)
    {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id=:id');
        $statement->bindValue(':id', $article->getId(), PDO::PARAM_INT);

        $statement->execute();
    }
    public function nombreVues(Article $article)
    {
        $statement = $this->connection->prepare("UPDATE article SET views=:views WHERE id=:id");
        $statement->bindValue(':id', $article->getId(), PDO::PARAM_INT);
        $statement->bindValue(":views", $article->getViews() + 1, PDO::PARAM_INT);
        $statement->execute();
    }

    public function findLikesByArticle(int $articleId): array
    {
        $likes = [];
        $statement = $this->connection->prepare('SELECT id_user FROM user_article WHERE id_article=:articleId');
        $statement->bindValue('articleId', $articleId);
        $statement->execute();
        $results = $statement->fetchAll();

        foreach ($results as $item) {
            $likes[] = $item['id_user'];
        }

        return $likes;
    }

    public function toggleLike(int $userId, int $articleId): bool
    {
        $statement = $this->connection->prepare('SELECT COUNT(*) FROM user_article WHERE id_user = :userId AND id_article = :articleId');
        $statement->bindValue('userId', $userId);
        $statement->bindValue('articleId', $articleId);
        $statement->execute();
        $likeCount = (int) $statement->fetchColumn();

        if ($likeCount > 0) {
            $deleteStatement = $this->connection->prepare('DELETE FROM user_article WHERE id_user = :userId AND id_article = :articleId');
            $deleteStatement->bindValue('userId', $userId);
            $deleteStatement->bindValue('articleId', $articleId);
            $deleteStatement->execute();

            return false;
        } else {
            $insertStatement = $this->connection->prepare('INSERT INTO user_article (id_user, id_article) VALUES (:userId, :articleId)');
            $insertStatement->bindValue('userId', $userId);
            $insertStatement->bindValue('articleId', $articleId);
            $insertStatement->execute();

            return true;
        }
    }

    public function findByPopularity(): array
    {
        $imageRepo = new ImageRepository();
        $categoryRepo = new CategoryRepository();
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article ORDER BY views DESC');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $images = $imageRepo->findImageByArticle($item['id']);
            $categories = $categoryRepo->findCategoryByArticle($item['id']);
            $article = $this->sqlToArticle($item);
            $article->setImages($images);
            $article->setCategories($categories);
            $likes = $this->findLikesByArticle($article->getId());
            $article->setLikes($likes);
            $articles[] = $article;
        }
        return $articles;
    }

}
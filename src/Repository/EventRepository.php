<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\User;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $events = [];
        $statement = $this->connection->prepare('SELECT * FROM event');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $events[] = $this->sqlToEvent($item);
        }
        return $events;
    }

    public function findById(int $id):?Event {
        $statement = $this->connection->prepare('SELECT * FROM event WHERE id=:id');
        $statement->bindValue(':id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToEvent($result);
        }
        return null;
    }

    private function sqlToEvent(array $line):Event {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Event($line['title'],$date,$line['adresse'], $line['content'],$line['image'], $line['id']);
    }

    public function persist(Event $event) {
        $statement = $this->connection->prepare('INSERT INTO event (title,date,adresse, content, image) VALUES (:title, :date, :adresse, :content, :image)');
        $statement->bindValue(':title', $event->getTitle());
        $statement->bindValue(':date', $event->getDate()->format('Y-m-d'));
        $statement->bindValue(':adresse', $event->getAdresse());$statement->bindValue(':content', $event->getContent());$statement->bindValue(':image', $event->getImage());

        $statement->execute();

        $event->setId($this->connection->lastInsertId());
    }


    public function update(Event $event)
    {
        $statement = $this->connection->prepare("UPDATE event SET title=:title,date=:date, adresse=:adresse, content=:content, image=:image WHERE id=:id");
        $statement->bindValue(":id", $event->getId(), PDO::PARAM_INT);
        $statement->bindValue(':title', $event->getTitle());
        $statement->bindValue(':date', $event->getDate()->format('Y-m-d'));
        $statement->bindValue(':adresse', $event->getAdresse());$statement->bindValue(':content', $event->getContent());$statement->bindValue(':image', $event->getImage());

        $statement->execute();
        return new JsonResponse(null, 204);
    }

    
    public function delete(Event $event) {
        $statement = $this->connection->prepare('DELETE FROM event WHERE id=:id');
        $statement->bindValue(':id', $event->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function subscribe(int $userId, int $eventId) {
        $statement = $this->connection->prepare("INSERT INTO user_event (id_user,id_event) VALUES (:idUser, :idEvent)");
        $statement->bindValue(':idUser', $userId);
        $statement->bindValue(':idEvent', $eventId);
        $statement->execute();
    }

    public function unsubscribe(int $userId, int $eventId) {
        $statement = $this->connection->prepare("DELETE FROM user_event WHERE id_user=:idUser and id_event = :idEvent");
        $statement->bindValue(':idUser', $userId);
        $statement->bindValue(':idEvent', $eventId);
        $statement->execute();
    }

    public function isSubscribe(int $userId, int $eventId) {
        $statement = $this->connection->prepare("SELECT * FROM user_event WHERE id_user=:idUser and id_event = :idEvent");
        $statement->bindValue(':idUser', $userId);
        $statement->bindValue(':idEvent', $eventId);
        $statement->execute();
        $result = $statement->fetch();  
        if($result) {
            return true;
        }
        return false;
     }
}


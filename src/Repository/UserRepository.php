<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\User;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $users = [];
        $statement = $this->connection->prepare('SELECT * FROM user');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $users[] = $this->sqlToUser($item);
        }
        return $users;
    }

    public function findById(int $id): ?User
    {
        $statement = $this->connection->prepare('SELECT * FROM user WHERE id=:id');
        $statement->bindValue(':id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            return $this->sqlToUser($result);
        }
        return null;
    }

    public function findByEmail(string $email): ?User
    {

        $query = $this->connection->prepare('SELECT * FROM user WHERE email=:email');
        $query->bindValue(':email', $email);
        $query->execute();
        $result = $query->fetch();
        if ($result) {
            return $this->sqlToUser($result);
        }
        return null;
    }

    private function sqlToUser(array $line): User
    {
        return new User($line['name'], $line['lastname'], $line['email'], $line['password'], $line['role'], $line['id']);
    }

    public function persist(User $user)
    {
        $statement = $this->connection->prepare('INSERT INTO user (name,lastname,email,password,role) VALUES (:name, :lastname, :email, :password, :role)');
        $statement->bindValue(':name', $user->getName());
        $statement->bindValue(':lastname', $user->getLastname());
        $statement->bindValue(':email', $user->getEmail());
        $statement->bindValue(':password', $user->getPassword());
        $statement->bindValue(':role', $user->getRole());

        $statement->execute();

        $user->setId($this->connection->lastInsertId());
    }


    public function update(User $user)
    {
        $statement = $this->connection->prepare("UPDATE user SET name=:name,lastname=:lastname,email=:email,password=:password,role=:role WHERE id=:id");
        $statement->bindValue(":id", $user->getId(), PDO::PARAM_INT);
        $statement->bindValue(':name', $user->getName());
        $statement->bindValue(':lastname', $user->getLastname());
        $statement->bindValue(':email', $user->getEmail());
        $statement->bindValue(':password', $user->getPassword());
        $statement->bindValue(':role', $user->getRole());

        $statement->execute();
        return new JsonResponse(null, 204);
    }


    public function delete(User $user)
    {
        $statement = $this->connection->prepare('DELETE FROM user WHERE id=:id');
        $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function findLikesByUser(int $userId)
    {


        $likes = [];
        $statement = $this->connection->prepare('SELECT *, article.id article_id, image.id image_id FROM user_article LEFT JOIN article ON user_article.id_article = article.id LEFT JOIN image ON image.id_article = article.id WHERE user_article.id_user = :userId');
        $statement->bindValue('userId', $userId);
        $statement->execute();

        /**
         * @var ?Article
         */
        $article = null;
        foreach ($statement->fetchAll() as $line) {
            if (empty($article) || $article->getId() != $line['article_id']) {
                $date = null;
                if (isset($line['date'])) {
                    $date = new DateTime($line['date']);
                }
                $article = new Article($line['title'], $line['content'], $line['author'], $date, $line['views'], $line['article_id']);
                $article->addImage($line['link'], $line['article_id']);
                $likes[] = $article;
            }

        }
        return $likes;
    }

    public function findEventsByUser(int $userId)
    {
        $statement = $this->connection->prepare('SELECT * FROM user_event LEFT JOIN event ON user_event.id_event = event.id WHERE user_event.id_user = :userId');
        $statement->bindValue('userId', $userId);
        $statement->execute();
        $results = $statement->fetchAll();
        $events = [];
        foreach ($results as $item) {
            $events[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'content' => $item['content'],
                'adresse' => $item['adresse'],
                'image' => $item['image'],
                'date' => $item['date']
            ];
        }
        return $events;
    }
}
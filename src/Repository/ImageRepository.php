<?php

namespace App\Repository;


use App\Entity\Image;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class ImageRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $images = [];
        $statement = $this->connection->prepare('SELECT * FROM image');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $images[] = $this->sqlToImage($item);
        }
        return $images;
    }

    public function findById(int $id):?Image {
        $statement = $this->connection->prepare('SELECT * FROM image WHERE id=:id');
        $statement->bindValue(':id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToImage($result);
        }
        return null;
    }

    private function sqlToImage(array $line):Image {
        return new Image($line['link'], $line['id_article'], $line['id']);
    }

    public function persist(Image $image) {
        $statement = $this->connection->prepare('INSERT INTO image (link, id_article) VALUES ( :link, :idArticle)');
        $statement->bindValue(':link', $image->getLink());
        $statement->bindValue(':idArticle', $image->getIdArticle(), PDO::PARAM_INT);
        $statement->execute();

        $image->setId($this->connection->lastInsertId());
    }


    public function update(Image $image)
    {
        $statement = $this->connection->prepare("UPDATE image SET link=:link,id_article=:idArticle WHERE id=:id");
        $statement->bindValue(":id", $image->getId(), PDO::PARAM_INT);
        $statement->bindValue(':link', $image->getLink());
        $statement->bindValue(':idArticle', $image->getIdArticle(), PDO::PARAM_INT);
        $statement->execute();
        return new JsonResponse(null, 204);
    }

    public function deleteById(int $id):bool
    {
       
        $statement = $this->connection->prepare('DELETE FROM image WHERE id=:id');
        $statement->bindValue(':id', $id);

        $results= $statement->execute();
        
        return $results;

    }

    /**
     * @return Image[]
     */
    public function findImageByArticle(int $id): array
    {
        $list = [];

        $query = $this->connection->prepare("
        SELECT * FROM image 
        WHERE image.id_article =:id");
        $query->bindValue(':id', $id);
        $query->execute();

        foreach ($query->fetchAll() as $line) {
            $list[] = new Image($line['link'], $line['id_article'], $line['id']);
        }

        return $list;
    }

}


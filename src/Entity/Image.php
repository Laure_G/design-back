<?php

namespace App\Entity;

class Image
{
    private ?int $id;
    private ?string $link;
    public ?int $idArticle;

    /**
     * @param int|null $id
     * @param string|null $link
     * @param int|null $idArticle
     */
    public function __construct( ?string $link, ?int $idArticle,?int $id=null) {
    	$this->id = $id;
    	$this->link = $link;
    	$this->idArticle = $idArticle;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getLink(): ?string {
		return $this->link;
	}
	
	/**
	 * @param string|null $link 
	 * @return self
	 */
	public function setLink(?string $link): self {
		$this->link = $link;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getIdArticle(): ?int {
		return $this->idArticle;
	}
	
	/**
	 * @param int|null $idArticle 
	 * @return self
	 */
	public function setIdArticle(?int $idArticle): self {
		$this->idArticle = $idArticle;
		return $this;
	}
}
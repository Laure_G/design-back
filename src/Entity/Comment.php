<?php

namespace App\Entity;
use DateTime;

class Comment
{
    private ?int $id;
    private ?DateTime $date;
    private ?string $content;
    private ?int $idArticle;
    private ?int $idUser;

	private ?string $author;

    /**
     * @param int|null $id
     * @param DateTime|null $date
     * @param string|null $content
     * @param int|null $idArticle
     * @param int|null $idUser
     */
    public function __construct( ?DateTime $date, ?string $content, ?int $idArticle, ?int $idUser, ?int $id=null) {
    	$this->id = $id;
    	$this->date = $date;
    	$this->content = $content;
    	$this->idArticle = $idArticle;
        $this->idUser = $idUser;
    }


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param  $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdArticle(): ?int {
		return $this->idArticle;
	}
	
	/**
	 * @param  $idArticle 
	 * @return self
	 */
	public function setIdArticle(?int $idArticle): self {
		$this->idArticle = $idArticle;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdUser(): ?int {
		return $this->idUser;
	}
	
	/**
	 * @param  $idUser 
	 * @return self
	 */
	public function setIdUser(?int $idUser): self {
		$this->idUser = $idUser;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param  $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
}
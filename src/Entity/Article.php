<?php

namespace App\Entity;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Article
{
    private ?int $id;
    #[Assert\NotBlank]
	private ?string $title;
	#[Assert\NotBlank]
    private ?string $content;
	#[Assert\NotBlank]
    private ?string $author;
	private ?DateTime $date;
    private ?int $views = 0;
	private ?array $images=[];
	private ?array $comments=[];
	private ?array $categories=[];
	private ?array $likes=[];
	private ?bool $userLiked = false;
	

	/**
	 * @param int|null $id
	 * @param string|null $title
	 * @param string|null $content
	 * @param string|null $author
	 * @param DateTime|null $date
	 * @param int|null $views
	 */
	public function __construct( ?string $title, ?string $content, ?string $author, ?DateTime $date, ?int $views,?int $id=null) {
		$this->id = $id;
		$this->title = $title;
		$this->content = $content;
		$this->author = $author;
		$this->date = $date;
		$this->views = $views;
	}


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param  $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param  $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param  $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getViews(): ?int {
		return $this->views;
	}
	
	/**
	 * @param  $views 
	 * @return self
	 */
	public function setViews(?int $views): self {
		$this->views = $views;
		return $this;
	}


	/**
	 * @return 
	 */
	public function getImages(): ?array {
		return $this->images;
	}
	
	/**
	 * @param  $images 
	 * @return self
	 */
	public function setImages(?array $images): self {
		$this->images = $images;
		return $this;
	}

	public function addImage(?string $link, ?int $idArticle)
    {
			$newImage = new Image ($link,$idArticle);
			array_push($this->images , $newImage);	
		
	}
	
    

	/**
	 * @return 
	 */
	public function getCategories(): ?array {
		return $this->categories;
	}
	
	/**
	 * @param  $categories 
	 * @return self
	 */
	public function setCategories(?array $categories): self {
		$this->categories = $categories;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getComments(): ?array {
		return $this->comments;
	}
	
	/**
	 * @param  $comments 
	 * @return self
	 */
	public function setComments(?array $comments): self {
		$this->comments = $comments;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getLikes(): ?array {
		return $this->likes;
	}
	
	/**
	 * @param  $likes 
	 * @return self
	 */
	public function setLikes(?array $likes): self {
		$this->likes = $likes;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getUserLiked(): ?bool {
		return $this->userLiked;
	}
	
	/**
	 * @param  $userLiked 
	 * @return self
	 */
	public function setUserLiked(?bool $userLiked): self {
		$this->userLiked = $userLiked;
		return $this;
	}
}
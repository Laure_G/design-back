<?php

namespace App\Entity;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Event
{
    private ?int $id;
    #[Assert\NotBlank]
	private ?string $title;
    #[Assert\NotBlank]
    private ?DateTime $date;
	#[Assert\NotBlank]
    private ?string $adresse;
	#[Assert\NotBlank]
    private ?string $content;
    private ?string $image;
   

	/**
	 * @param int|null $id
	 * @param string|null $title
	 * @param DateTime|null $date
     * @param string|null $adresse
     * @param string|null $content
     * @param string|null $image
	 */
	public function __construct( ?string $title, ?DateTime $date,?string $adresse, ?string $content,?string $image,?int $id=null) {
		$this->id = $id;
		$this->title = $title;
		$this->date = $date;
        $this->adresse = $adresse;
        $this->content = $content;
        $this->image = $image;
	}

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param  $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getAdresse(): ?string {
		return $this->adresse;
	}
	
	/**
	 * @param  $adresse 
	 * @return self
	 */
	public function setAdresse(?string $adresse): self {
		$this->adresse = $adresse;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param  $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param  $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
}
<?php

namespace App\Entity;
use DateTime;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    private ?int $id;
    #[Assert\NotBlank]
	private ?string $name;
    #[Assert\NotBlank]
    private ?string $lastname;
	#[Assert\NotBlank]
    private ?string $email;
	#[Assert\NotBlank]
    private ?string $password;
    private ?string $role;
	private ?array $likedArticles=[];

 
    public function __construct( ?string $name, ?string $lastname, ?string $email,?string $password,?string $role,?int $id=null) {
		$this->id = $id;
		$this->name = $name;
		$this->lastname = $lastname;
		$this->email = $email;
		$this->password = $password;
		$this->role = $role;
	}

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getLastname(): ?string {
		return $this->lastname;
	}
	
	/**
	 * @param  $lastname 
	 * @return self
	 */
	public function setLastname(?string $lastname): self {
		$this->lastname = $lastname;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEmail(): ?string {
		return $this->email;
	}
	
	/**
	 * @param  $email 
	 * @return self
	 */
	public function setEmail(?string $email): self {
		$this->email = $email;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPassword(): ?string {
		return $this->password;
	}
	
	/**
	 * @param  $password 
	 * @return self
	 */
	public function setPassword(?string $password): self {
		$this->password = $password;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getRole(): ?string {
		return $this->role;
	}
	
	/**
	 * @param  $role 
	 * @return self
	 */
	public function setRole(?string $role): self {
		$this->role = $role;
		return $this;
	}

    public function getRoles(): array {
        return [$this->role];
	}

    public function eraseCredentials() {
	}
	
	public function getUserIdentifier(): string {
        return $this->email;
    }

	/**
	 * @return 
	 */
	public function getLikedArticles(): ?array {
		return $this->likedArticles;
	}
	
	/**
	 * @param  $likedArticles 
	 * @return self
	 */
	public function setLikedArticles(?array $likedArticles): self {
		$this->likedArticles = $likedArticles;
		return $this;
	}


}
-- Active: 1673948009961@@127.0.0.1@3306@design

DROP TABLE IF EXISTS user_event;
DROP TABLE IF EXISTS user_article;
DROP TABLE IF EXISTS category_event;
DROP TABLE IF EXISTS category_article;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS article;

CREATE TABLE article (
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        content VARCHAR(10000) NOT NULL,
        author VARCHAR(255) NOT NULL,
        date DATETIME,
        views INT
    );

CREATE TABLE category (
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL
    );

CREATE TABLE image (
        id INT PRIMARY KEY AUTO_INCREMENT,
        link VARCHAR(1000) NOT NULL,
        id_article INT,
        FOREIGN KEY (id_article) REFERENCES article(id) ON DELETE SET NULL
    );

CREATE TABLE user (
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        lastname VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        role VARCHAR(255) NOT NULL
    );

CREATE TABLE event (
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        date DATETIME,
        adresse VARCHAR(255) NOT NULL,
        content VARCHAR(6000) NOT NULL,
        image VARCHAR(1000) NOT NULL
    );

CREATE TABLE comment (
        id INT PRIMARY KEY AUTO_INCREMENT,
        date DATETIME,
        content VARCHAR(1000) NOT NULL,
        id_article INT,
        id_user INT,
        FOREIGN KEY (id_article) REFERENCES article(id) ON DELETE SET NULL,
        FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE SET NULL
    );


CREATE TABLE category_article (
    id_category INT,
    id_article INT,
    PRIMARY KEY (id_category, id_article),
    Foreign Key (id_category) REFERENCES category(id) ON DELETE CASCADE,
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE
);

CREATE TABLE category_event (
    id_category INT,
    id_event INT,
    PRIMARY KEY (id_category, id_event),
    Foreign Key (id_category) REFERENCES category(id) ON DELETE CASCADE,
    Foreign Key (id_event) REFERENCES event(id) ON DELETE CASCADE
);

CREATE TABLE user_article (
    id_user INT,
    id_article INT,
    PRIMARY KEY (id_user, id_article),
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE
);

CREATE TABLE user_event (
    id_user INT,
    id_event INT,
    PRIMARY KEY (id_user, id_event),
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    Foreign Key (id_event) REFERENCES event(id) ON DELETE CASCADE
);

INSERT INTO article (title, content, author, date, views) VALUES ("Les tours d'Oscar Niemeyer", "Les tours d'Oscar Niemeyer au palais Itamaraty à BrasiliaCe bâtiment de béton conçu en 1970 par Oscar Niemeyer héberge le ministère des Affaires étrangères du Brésil. D'une beauté sculpturale, l'escalier semble presque flotter en l'absence de garde-corps et de contre-marches, ses marches très fines se déployant en éventail dans une courbe tout en fluidité. Les tours d'Oscar Niemeyer au palais Itamaraty à BrasiliaCe bâtiment de béton conçu en 1970 par Oscar Niemeyer héberge le ministère des Affaires étrangères du Brésil. D'une beauté sculpturale, l'escalier semble presque flotter en l'absence de garde-corps et de contre-marches, ses marches très fines se déployant en éventail dans une courbe tout en fluidité.Les tours d'Oscar Niemeyer au palais Itamaraty à BrasiliaCe bâtiment de béton conçu en 1970 par Oscar Niemeyer héberge le ministère des Affaires étrangères du Brésil. D'une beauté sculpturale, l'escalier semble presque flotter en l'absence de garde-corps et de contre-marches, ses marches très fines se déployant en éventail dans une courbe tout en fluidité.Les tours d'Oscar Niemeyer au palais Itamaraty à BrasiliaCe bâtiment de béton conçu en 1970 par Oscar Niemeyer héberge le ministère des Affaires étrangères du Brésil. D'une beauté sculpturale, l'escalier semble presque flotter en l'absence de garde-corps et de contre-marches, ses marches très fines se déployant en éventail dans une courbe tout en fluidité.Les tours d'Oscar Niemeyer au palais Itamaraty à BrasiliaCe bâtiment de béton conçu en 1970 par Oscar Niemeyer héberge le ministère des Affaires étrangères du Brésil. D'une beauté sculpturale, l'escalier semble presque flotter en l'absence de garde-corps et de contre-marches, ses marches très fines se déployant en éventail dans une courbe tout en fluidité.","Blandine Frisina", "2023-08-30",1),("Ice de Las Pozas", "Ice de Las Pozas au Mexique. En plein milieu de la jungle mexicaine, à Xilitla, le poète britannique Edward James a réalisé, entre 1962 et 1984, un jardin surréaliste de 32 hectares ponctué d'une trentaine de folies. Les sculptures en béton se fondent dans la végétation environnante au milieu des cascades naturelles et des étangs. En plein cœur de cet éden, l'Escalier vers le paradis apparaît dans ce décor comme une vision fantasmagorique.","Juiletta Simon", "2023-08-29 17:00",3),
("La villa Baori","La villa Baori en IndeCreusé au VIIIe siècle à la demande du roi Chanda, ce vaste puits à degrés situé dans l’état du Rajasthan a été bâti sur un plan carré et une profondeur de 30 mètres, de façon à recueillir l’eau de pluie pendant la mousson. Ses 3500 marches réparties sur treize étages en font un lieu surprenant à l’esthétique cinégénique. Pas étonnant que Christopher Nolan ait choisi cet endroit pour en faire la prison du héros dans The Dark Knight Rises, dernier volet de la trilogie Batman.","Edward Lone", '2023-08-28 17:00',2), 
("Le vestibule de Bramante","Le vestibule de Bramante à RomeComme son nom ne l’indique pas, ce majestueux escalier à double hélice qui sert de rampe d'accès aux musées du Vatican n’est pas l’œuvre de Donato d’Angelo dit Bramante, mais de l’architecte Giuseppe Momo en 1932. Bramante a bien réalisé un double escalier en spirale semblable à celui-ci en 1512 qui se trouve encore aujourd’hui au Vatican, dans la cour du Belvédère.","J-F Champollion", '2023-08-27 15:00',4),
("L'escalier de la boutique Olivetti", "L'escalier de la boutique Olivetti à Venise.C'est en 1957 que l’entreprise confie l’architecture de sa boutique place Saint-Marc à l'architecte vénitien Carlo Scarpa. Restaurée en 2011 grâce au FAI (fonds pour l'environnement italien) et transformée en musée, elle ouvre au public qui découvre alors ce joyau architectural. Élément central de l'espace, l’escalier en pierre d’Istrie rappelle, avec ses marches asymétriques, les retours chariot des machines à écrire.","James Meziou", "2023-08-26",3),
("L'Alberaria", "L'Alberaria à Venise, connue également sous le nom de Torre di Porta Nuova, cette tour de 35 mètres de hauteur datant du début du XIXe siècle, située au sein de l'arsenal de Venise, abrite un escalier doté d'un impressionnant mécanisme de roue maintenue entre deux murs épais. Restructurée par le studio d'architectes MAP Studio, la tour a réouvert au public en 2011 et fait désormais partie du parcours de la Biennale.","Nicolas Dupont", "2023-08-25",3),
("Le musée des Confluences", "Le musée des Confluences. L’escalier à doubles révolutions est sans nul doute l’élément architectural le plus marquant du château, et sa fascination depuis le XVIe siècle ne s’est jamais démentie. Sa place au centre de l’immense donjon carré le met en effet en scène d’une manière inédite : autour de son noyau central, deux rampes d’escalier s’enroulent l’une au-dessus de l’autre afin de desservir les étages principaux, tout en permettant à deux personnes l'empruntant de ne jamais se rencontrer. Une merveille d’inventivité influencée par le travail de Léonard de Vinci.","Marie Sevy", "2023-08-24 17:00",3),("L'escalier de l'envolée cosmique", "L'escalier de l'envolée cosmique en Écosse. Cette impressionnante volée de marches sombres zigzaguant à travers le Jardin de la spéculation cosmique, près de Glasgow. Très éloigné du jardin à l’anglaise tel qu’on se le représente, cet endroit étonnant de 15 hectares accueille, en lieu et place des traditionnels bosquets et autres parterres, des sculptures inspirées de la théorie mathématique du chaos et de la géométrie fractale. Tout un programme… initié par l’architecte paysagiste et théoricien Charles Jencks.","John Doe", "2022-08-23",2),("Le musée Gugghenheim", "Le musée Gugghenheim à Bilbao. À partir de la salle des pas perdus, on accède aux niveaux supérieur et inférieur du Palais d'Iéna par un escalier monumental en béton à double révolution pensé par l'architecte Auguste Perret. Pièce maîtresse de l'édifice, cet escalier est une véritable prouesse car si, à première vue il semble ne reposer sur rien et paraît presque s'envoler en une arabesque aérienne, son coffrage a nécessité de contourner de nombreuses difficultés techniques.","Déborah Martinez", "2023-08-22",5),("Les voûtes de la Quinta da Regaleira", "Les voûtes de la Quinta da Regaleira près de LisbonneÀ Sintra, près de Lisbonne, la Quinta da Regaleira est un palais construit au début du XXe siècle par le millionnaire António Augusto Carvalho Monteiro. Il abrite le Puits initiatique, une tour inversée de 27 mètres dotée d'un escalier en spirale censé mener des ténèbres vers la lumière en passant par neuf paliers, métaphore des neuf cercles de la Divine Comédie de Dante.","Alexandre Legrand", "2023-08-21 17:00",2), ("L'appartement de Peñón de Guatapé ", "L'appartement de Peñón de Guatapé en Colombie. Situé près de Medellin, le Peñón de Guatapé est un monolithe de 220 mètres de haut qui contient plusieurs fissures, dont une où un escalier de 740 marches zigzague jusqu'au sommet offrant un panorama sur toute la région d'Antioquia.","Blandine Frisina", "2023-08-20",1);

INSERT INTO category (name) VALUES ("Architecture"), ("Site archéologique"),("Site historique"),("Design"), ("Exposition"),("Street Art"), ("Musée"), ("Art"), ("Cinéma"), ("Jeux vidéos");

INSERT INTO image (link, id_article) VALUES ("https://images.unsplash.com/photo-1560840067-ddcaeb7831d2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2879&q=80", 1), ("https://images.unsplash.com/photo-1517732985480-66abf3dbf26e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2970&q=80", 1), ("https://images.unsplash.com/photo-1636892004545-102ebad8bfb7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3149&q=80", 1),("https://images.unsplash.com/photo-1551042653-cf6987934d28?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3129&q=80", 1), ("https://images.unsplash.com/photo-1649513137940-daacab3ee11f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2448&q=80",2),("https://images.unsplash.com/photo-1618220179428-22790b461013?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3027&q=80",3), ("https://images.unsplash.com/photo-1500462918059-b1a0cb512f1d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3087&q=80", 4), ("https://images.unsplash.com/photo-1548318281-7da3085ce691?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2241&q=80",5),("https://images.unsplash.com/photo-1495985812444-236d6a87bdd9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2970&q=80", 6),("https://images.unsplash.com/photo-1483959651481-dc75b89291f1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2470&q=80",7), ( "https://images.unsplash.com/photo-1506749547535-fe88ffa57a7a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3174&q=80",8),("https://images.unsplash.com/photo-1477140765885-9469f102a270?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2970&q=80",9), ("https://images.unsplash.com/photo-1465491736982-abaddedcedc7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2970&q=80",10), ("https://images.unsplash.com/photo-1606744824163-985d376605aa?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3132&q=80",11);

INSERT INTO user (name, lastname, email, password, role) VALUES ( 'Marie', 'Test','marie@gmail.com','marie2', 'ROLE_ADMIN'), ('Julie', 'Garcia','julie@gmail.com','julie1','ROLE_USER'), ('Luc', 'Ricochet','luc@gmail.com','luc2', 'ROLE_USER');

INSERT INTO event (title, date, adresse, content, image) VALUES ("Sparkle lights","2023-08-20","Musée des beaux arts de Lyon",'Peintre quasi maudit aux États-Unis, Robert Guinan a connu plus de succès (très relatif) en Europe à partir de la fin des années 1970. Le Musée des Beaux-Arts a eu la bonne idée de nous faire redécouvrir cet artiste des bas-fonds de Chicago à travers une rétrospective qui prend aux tripes.', "https://images.unsplash.com/photo-1584891800728-d4bc8e6f7c2e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2624&q=80"), ("Néons Art","2023-08-26","Lyon",'Depuis sa première édition, en 1980, qui, sous l’intitulé « La Présence du passé », avait ouvert la voie au postmodernisme en Europe, la Biennale d’architecture de Venise s’est toujours présentée comme une invitation à l’introspection. Dans les lieux chargés d’histoire où elle se déploie, entre les halles de l’Arsenal et les pavillons des Giardini, la célébration de la discipline et de ses valeureux représentants va de pair avec une forme d’autocritique et une aspiration au renouvellement. L’exercice est périlleux, et les écueils nombreux : espaces gigantesques, commissaires souvent inexpérimentés, dépassés par l’ampleur de la mission, quand ils ne la conçoivent pas comme un pur exercice de diplomatie, architectes qui se prennent pour des artistes ou des universitaires… Mais Lesley Lokko, la commissaire de cette dix-huitième édition, qui s’est ouverte samedi 20 mai, les a évités. Elle s’en tire même avec panache.', "https://images.unsplash.com/photo-1559329311-d12f0b9d770d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2971&q=80"),("ArchiColor","2023-09-12","Lyon",'Depuis sa première édition, en 1980, qui, sous l’intitulé « La Présence du passé », avait ouvert la voie au postmodernisme en Europe, la Biennale d’architecture de Venise s’est toujours présentée comme une invitation à l’introspection. Dans les lieux chargés d’histoire où elle se déploie, entre les halles de l’Arsenal et les pavillons des Giardini, la célébration de la discipline et de ses valeureux représentants va de pair avec une forme d’autocritique et une aspiration au renouvellement. L’exercice est périlleux, et les écueils nombreux : espaces gigantesques, commissaires souvent inexpérimentés, dépassés par l’ampleur de la mission, quand ils ne la conçoivent pas comme un pur exercice de diplomatie, architectes qui se prennent pour des artistes ou des universitaires… Mais Lesley Lokko, la commissaire de cette dix-huitième édition, qui s’est ouverte samedi 20 mai, les a évités. Elle s’en tire même avec panache.', "https://images.unsplash.com/photo-1502691876148-a84978e59af8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2340&q=80"),("Contemporary Arty","2023-12-20","Lyon",'Depuis sa première édition, en 1980, qui, sous l’intitulé « La Présence du passé », avait ouvert la voie au postmodernisme en Europe, la Biennale d’architecture de Venise s’est toujours présentée comme une invitation à l’introspection. Dans les lieux chargés d’histoire où elle se déploie, entre les halles de l’Arsenal et les pavillons des Giardini, la célébration de la discipline et de ses valeureux représentants va de pair avec une forme d’autocritique et une aspiration au renouvellement. L’exercice est périlleux, et les écueils nombreux : espaces gigantesques, commissaires souvent inexpérimentés, dépassés par l’ampleur de la mission, quand ils ne la conçoivent pas comme un pur exercice de diplomatie, architectes qui se prennent pour des artistes ou des universitaires… Mais Lesley Lokko, la commissaire de cette dix-huitième édition, qui s’est ouverte samedi 20 mai, les a évités. Elle s’en tire même avec panache.', "https://images.unsplash.com/photo-1640123285578-6f3efeb3685b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2274&q=80") ;

INSERT INTO comment (date, content, id_article, id_user) VALUES("2023-01-24 20:00","Incroyable lieu à voir au moins une fois dans sa vie",1,2),("2023-01-27 15:00'","Un design exceptionnel", 2, 3) ;

INSERT INTO category_article (id_category, id_article) VALUES (1,1),(4,3),(6,2),(7,4),(4,5),(2,6),(7,7),(1,8),(7,9),(2,10),(4,11) ;

INSERT INTO category_event (id_category, id_event) VALUES (1,2),(5,1),(7,3),(5,4);

INSERT INTO user_article (id_user, id_article) VALUES (1,2),(2,1),(3,1),(2,2),(1,5),(2,7),(3,9),(2,10);

INSERT INTO user_event (id_user, id_event) VALUES (1,2),(2,1),(3,1),(2,2);
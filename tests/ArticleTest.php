<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleTest extends WebTestCase
{

    public function testGetAll()
    {

        $client = static::createClient();

        $client->request('GET', '/api/article');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $item = $body[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertIsInt($item['id']);
        $this->assertArrayHasKey('title', $item);
        $this->assertIsString($item['title']);
        $this->assertArrayHasKey('content', $item);
        $this->assertIsString($item['content']);
        $this->assertArrayHasKey('author', $item);
        $this->assertIsString($item['author']);
        $this->assertArrayHasKey('date', $item);
        new \DateTime($item['date']);
        $this->assertArrayHasKey('views', $item);
        $this->assertIsInt($item['views']);

    }

}